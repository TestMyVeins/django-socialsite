# Portal społecznościowy

## Od początku

Zainstaluj Phythona w wersji 3.10+ a następnie wykonaj poniższe polecenia po kolei:

`$ pip install virtualenv`

`$ virtualenv venv`

`$ source venv/Scripts/activate` | `$ source venv/bin/activate`

`$ pip install -r requirements.txt`

`$ pre-commit install`

`$ python manage.py migrate`

`$ python manage.py createsuperuser`

Wpisz dane swojego użytkownika

___

### Uruchamianie:

`$ source venv/Scripts/activate`

`$ python manage.py runserver`

### Aby wyjść z środowiska: 

`$ deactivate`

___